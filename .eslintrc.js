module.exports = {
    root: true,
    parserOptions: {
        ecmaVersion: 2020,
        sourceType: 'module'
    },
    plugins: [
        'ember'
    ],
    extends: [
        'eslint:recommended',
        'plugin:ember/recommended'
    ],
    env: {
        browser: true
    },
    globals: {
        Uint8Array
    },
    rules: {
        'arrow-parens': 0,
        'ember/no-attrs-in-components': 0,
        'ember/no-capital-letters-in-routes': 0,
        'ember/routes-segments-snake-case': 0,
        'new-cap': 0,
        'no-console': ['error', {
            allow: ['warn', 'error']
        }],
        'no-var': 'error'
    }
};