class Commands {
  constructor(args) {
    if (!args || typeof args !== 'object') throw new Error(`Invalid constructor param for CommandArgs: ${args}`);
    this.workingPath = args[1];
    this.args = args.slice(2);
    this.map = this.GenArgMap(args);
  }

  GenArgMap(args) {
    const pairs = [];
    args.forEach((arg, i) => {
      if (arg.includes('--') && args[i + 1]) {
        pairs.push([arg, args[i + 1]]);
      }
    });
    console.log(pairs);
    return new Map(pairs);
  }
}

// let commands = (function (args) {
//   const _args = args.splice(2);

//   const getArgPairs = () => {
//     let argValues = {};
//     _args.forEach((arg, i) => {
//       if (arg.includes('--') && args[i + 1]) {
//         argValues[arg] = _args[i + 1];
//       }
//     });

//     return argValues;
//   }
//   return getArgPairs
// }(args));

module.exports = Commands;