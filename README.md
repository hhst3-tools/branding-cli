# README #

CLI tool used for client career site (brandings) autoamtion, development, and workflow

### What is this repository for? ###

* CLI tool that uses gulp.js to run a series of task to automate the cleaining and developement of career sites for HHS clients.
* Version: 1.00

### How do I get set up? ###

* install gulp [Gulp JS Task Runner](https://gulpjs.com/docs/en/getting-started/quick-start)
* npm install


### Who do I talk to? ###

* Sam Jones <Sam.Jones@healthcaresource.com>
* Amanda Oliverio <amanda.oliverio@healthcaresource.com>