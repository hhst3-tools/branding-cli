


const {
    src,
    dest,
    task,
  } = require('gulp');
const cheerio = require('gulp-cheerio');
const fs = require('fs-extra');
const path = require('path');


task('clean-html-css-namespaces', async (done) => {
  await new Promise(resolve => {
    const srcFolder = "branding/";
    const themeFolder = path.resolve(srcFolder, 'theme');
    const cssFile = path.resolve(themeFolder, 'style.css');
    let css = fs.readFileSync(cssFile, 'utf8');
    src(['branding/*.htm', 'branding/*.html'])
    .pipe(cheerio(function ($, file, cb) {
    
        $('[id]').each(function() {
            let id = this.attribs.id;
      
            if (id === 'rootDiv') {
              // Ignore this id
            } else if (css.indexOf(`#${id}`) < 0 && css.indexOf(`#vc-${id}`) < 0) {
              $(this).removeAttr('id');
              console.info(`Removing unused id ${id}`);
            } else if (id.indexOf('vc-') !== 0) {
              $(this).attr('id', `vc-${id}`);
              css = css.replace(new RegExp(`#${id}`, 'g'), `#vc-${id}`);
            }
          });

          let classNames = [];
          $('[class]').each(function() {
            let classes = this.attribs.class.split(' ');
      
            for (let className of classes) {
              // Ignore whitespaces
              if (className.length === 0 || /^\s+$/.test(className)) {
                continue;
              }
      
              // Ignore existing namespaced classes
              if (className.indexOf('vc-') === 0 && css.indexOf(`.${className}`) > -1) {
                continue;
              }
      
              $(this).removeClass(className);
      
              //Checking for classes that are not used
              if (css.indexOf(`.${className}`) < 0 && css.indexOf(`.vc-${className}`) < 0 && css.indexOf(`.vc-${className}`)) {
                console.info(`Removing unused class ${className}`);
                continue;
              }
              
              //Namespace CSS
              if (classNames.indexOf(className) < 0) {
                classNames.push(className);
                css = css.replace(new RegExp(`\\.${className}`, 'g'), `.vc-${className}`);
              }
              
              //Namespace HTML
              $(this).addClass(`vc-${className}`);
            }
          });
        
        // Remove multiple empty lines
        css = css.replace(/^\s*[\r\n]/gm, '');

        fs.writeFileSync(cssFile, css);
      cb();
    }))
    .pipe(dest('branding/'))
    .on('finish', resolve);
  });


  done();
});