const { task, src, dest } = require("gulp");
const cheerio = require('gulp-cheerio');
const fs = require('fs-extra');
const path = require('path');


task('css-in-theme', async(done) => {
    await new Promise((resolve, reject) => {
        src(['branding/*.htm', 'branding/*.html'])
        .pipe(cheerio(($, file, cb) => {
            let cssFiles = [];
            $('link[href]').each((_, el) => {
              let href = el.attribs.href;
        
              if (el.attribs.rel !== 'stylesheet') {
                validHtml = false;
                console.error( `Found non stylesheet link: ${href}`);
              } else if (href.indexOf('theme/') !== 0 && href.indexOf('fonts') === -1) {
                validHtml = false;
                console.error( `Found stylesheet outside theme: ${href}`);
              } else if (href.indexOf('@') !== -1) {
                validHtml = false;
                console.error( `Found invalid stylesheet link: ${href}`);
              } else {
                cssFiles.push(href);
              }
            });

            let files = fs.readdirSync(path.resolve(process.cwd(), 'branding/theme'));
            files.forEach(file => {
              if (file.indexOf('.css') > -1) {
                if (cssFiles.indexOf(`theme/${file}`) === -1) {
                  validHtml = false;
                  console.error( `Founds unimported css file: ${file}`);
                } 
              }
            });

            cb();
        }))
        .on('error', reject)
        .on('finish', resolve)
    })
    done();
});
