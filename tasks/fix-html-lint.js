
const fs = require('fs');
const {
  src,
  dest,
  task
} = require('gulp');
const htmlmin = require('gulp-htmlmin');



task('fix-html-lint', async (done) => {
    await new Promise((resolve, reject) => {
        src('branding/template.htm')
        .pipe(htmlmin({ collapseWhitespace: true,
            preserveLineBreaks: true,
            removeComments: true,
            removeCommentsFromCDATA: true,
            removeCDATASectionsFromCDATA: true,
            removeRedundantAttributes: true,
            useShortDoctype: true,
            removeEmptyAttributes: true }))
        .pipe(dest('branding/'))
        .on('error', reject)
        .on('finish', resolve());
      });
    done();
});