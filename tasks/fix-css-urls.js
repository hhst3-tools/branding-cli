const editCssUrls = require('gulp-modify-css-urls');
const {
  src,
  dest,
  task
} = require('gulp');

let modifyCssUrls = async (cb) => {
  await new Promise((resolve, reject) => {
    src(['branding/theme/*.css'])
      .pipe(editCssUrls({
        modify: function (cssurl, filePath) {
          let urlArray = cssurl.split('/');
          let fileName = urlArray[urlArray.length - 1]

          return fileName;
        }
      }))
      .pipe(dest('branding/theme'))
      .on('error', reject)
      .on('finish', resolve);
  })
  cb();
}

task('fix-css-urls', modifyCssUrls);