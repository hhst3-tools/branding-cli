const { task, src, series } = require("gulp");
const through2 = require('through2');
const ListStream = require('list-stream');
var validate = require('gulp-w3c-css');


task('validate-stylecss-exist', async (done) => {
    await new Promise((resolve, reject) => {
        src(['branding/theme/style.css'])
        .pipe(through2.obj((file, _, cb) => {
            if(!file.contents) throw new Error("Style css missing");


            cb();
        }))
        .on('error', reject)
        .on('finish', resolve)
    })
    done();
});

task('validate-w3c', async (done) => {
    await new Promise((resolve, reject) => { 
    src(['branding/theme/style.css'])
        .pipe(validate({usermedium: 'screen'}))
        .pipe(ListStream.obj((err, files) => {
            if(err) throw new Error(err);
            var json = JSON.parse(files[0].contents);
            console.log(json.errors);
            
        }))
        
        .on('error', reject)
        .on('finish', resolve);
    })
    done();  
})

task('validate-css', series('validate-stylecss-exist','validate-w3c'))