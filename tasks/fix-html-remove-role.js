/* eslint-disable no-restricted-syntax */
const cheerio = require('gulp-cheerio');
const {
  src,
  dest,
  task,
  series,
} = require('gulp');

let stripHtml = async (done) => {
  await new Promise((resolve, reject) => {
    src(['branding/*.htm', 'branding/*.html'])
      .pipe(cheerio(($, file, cb) => {
        $('*').each((_, el) => {
            $(el).removeAttr('role', null);
        }); 
        cb();
      }))
      .pipe(dest('branding/'))
      .on('error', reject)
      .on('finish', resolve);
  })
  done();
}

task('remove-roles', stripHtml);