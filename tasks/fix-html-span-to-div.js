/* eslint-disable no-restricted-syntax */
const cheerio = require('gulp-cheerio');
const {
  src,
  dest,
  task,
  series,
} = require('gulp');

let stripHtml = async done => {
  await new Promise((resolve, reject) => {
    src(['branding/*.htm', 'branding/*.html'])
      .pipe(cheerio(($, file, cb) => {
      
        const phrasingTags = ['abbr', 'aduio', 'b', 'bdo', 'br', 'button', 'canvas', 'code', 'command', 'data', 'datalist', 'dfn', 'em', 'embed', 'i', 'iframe', 'img', 'input', 'kbd', 'keygen', 'label', 'mark', 'math', 'meter', 'noscript', 'object', 'output', 'picture', 'progress', 'q', 'ruby', 'samp', 'script', 'select', 'small', 'span', 'strong', 'sub', 'sup', 'svg', 'textarea', 'time', 'var', 'video', 'wbr']

        let spanInvalid = false
        //let childSelector =`${tag}:first-of-type`
          $('span').each((_, el) => {
            $(el).children().each((i, child) => {
              //console.log(child.fi);
            })             
          });

        cb();
      }))
      .pipe(dest('branding/'))
      .on('error', reject)
      .on('finish', resolve);
  })
  done();
}

task('fix-html-span-to-div', stripHtml);