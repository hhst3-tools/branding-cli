const {
    src,
    dest,
    task
  } = require('gulp');
const PostCSS = require("gulp-postcss");
const PrefixWrap = require("postcss-prefixwrap");

task('fix-css-wrap-css-rules', async (done) => {
    await new Promise((resolve, reject) => {
    src(['branding/theme/*.css'])
        .pipe(PostCSS([PrefixWrap(".vc-hhs")]))
        .pipe(dest("branding/theme/"))
        .on('error', reject)
        .on('finish', resolve)
    });
    done();
});