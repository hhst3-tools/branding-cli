'use strict';
const cheerio = require('gulp-cheerio');
const {
  src,
  dest,
  task
} = require('gulp');
const fs = require('fs-extra');

async function fixHtmlLinks(done) {
  let url = fs.readFileSync('branding/info.txt').toString();
  console.log(url);
  if(!url.endsWith("/")) {
    url = `${url}/`
  }

  await new Promise((resolve, reject) => {
    src(['branding/*.htm', 'branding/*.html'])
      .pipe(cheerio(function ($, file, cb) {
        $('a[href]').each((_, el) => {
          var relativeUrl = el.attribs.href;
          if(el.attribs.href.startsWith('mailto:') || el.attribs.href.startsWith('tel:') ) {
              $(el).remove();
          } else if ( !el.attribs.href.startsWith('http') && el.attribs.href !== '/') {
            if(el.attribs.href.startsWith("//")) {
              if (url.startsWith('https')) {
                $(el).attr('href', `https:${relativeUrl}`)
              } else {
                $(el).attr('href', `http:${relativeUrl}`)
              }
            } else if(relativeUrl.startsWith("/") || relativeUrl.startsWith("./") || relativeUrl.startsWith("#")) {
                $(el).attr('href', `${url}${relativeUrl.substr(2)}`);
            } else if (relativeUrl.startsWith("#")) {
              $(el).attr('href', `${url}`);
            }
          }
      });
        cb();
      }))
      .pipe(dest('branding/'))
      .on('error', reject)
      .on('finish', resolve);

  });

  done();
}

task('fix-html-links', fixHtmlLinks);