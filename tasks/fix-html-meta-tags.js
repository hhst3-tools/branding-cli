const { task, src, dest } = require("gulp");
const cheerio = require('gulp-cheerio');

task('fix-html-meta-tags', async(done) => {
    await new Promise((resolve, reject) => {
        src(['branding/*.htm', 'branding/*.html'])
        .pipe(cheerio(($, file, cb) => {
            $('meta').each((_, el) => {
                $(el).remove();
            })

            $('head').prepend('<meta charset="utf-8">');
            cb();
        })).pipe(dest('branding/'))
        .on('error', reject)
        .on('finish', resolve)
    })
    done();
});
