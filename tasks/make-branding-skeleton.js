const {
  src,
  dest,
  task
} = require('gulp');

async function makeBrandingSkeleton(done) {
  await new Promise((resolve, reject) => {
    src(['required/**/*'])
      .pipe(dest('branding'))
      .on('error', reject)
      .on('finish', resolve);
  });

  done();
}

task('make-branding-skeleton', makeBrandingSkeleton);