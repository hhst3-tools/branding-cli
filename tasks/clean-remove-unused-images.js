const {
    task,
    src,
    dest
  } = require('gulp');
  const del = require('del');
  var unusedImages = require('gulp-unused-images');
  var plumber = require('gulp-plumber');
  var path = require('path');
  var through2 = require('through2');
  var fs = require('fs-extra');

  task('clean-remove-unused-images', async (done) => {
    await new Promise((resolve, reject) => {
     
        src(['branding/theme/*.jpg', 'branding/theme/*.png', 'branding/theme/*.gif', 'branding/theme/*.svg' ])
        .pipe(through2.obj((file, _, cb) => {
            var imgName = file.relative;
            var imgPath = file.path;
            let html = fs.readFileSync('branding/template.htm', 'utf8');
            let css = fs.readFileSync('branding/theme/style.css', 'utf8');
       
            if (html.indexOf(imgName) < 0 && css.indexOf(imgName) < 0) {
                 fs.removeSync(imgPath);
                 console.info(`Removing unused image ${imgName}`);
             }


            cb();
        }))
        .on('error', reject)
        .on('finish', resolve)
    })
    done();
});


