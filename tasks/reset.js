const del = require('del');
const {
  src,
  dest,
  task,
  series
} = require('gulp');

/**********************************************************************************
 * Task: delete-branding
 * input: done (signals async completion)
 * output: resolved promise
 * description: awaits promise that calls del and loops through the branding folder
 *              deleting everything except the ./git folder
 *              done() signals async completing top gulp; 
 **********************************************************************************/

task('delete-branding', async (done) => {
  await new Promise(resolve => {
    del([
        './branding/theme/**/*',
        '!./branding/.git',
        './branding/template.htm',
        './branding/info.txt',
        './branding/variables.scss'
      ])
      .then(resolve)
  });

  done();
})

/************************************************************************************************************
 * Task: reset-branding
 * input: done (signals asycn completion)
 * output: resolved promise
 * description: awaits promise - src() pipes in all folders and files in the "/branding-worflow/holding/" folder to 
 *              and writes a copy using dest() to the "/branding-workflow/branding/" folder
 *************************************************************************************************************/

task('reset-branding', async (done) => {
  await new Promise(resolve => {
    src(['holding/**/*'])
      .pipe(dest('branding'))
      .on('finish', resolve);
  })

  done();
});

/*******************************************************************************************
 * Task: reset
 * input: 
 * output:
 * description: calls series which executes 'delete-branding' and 'reset-branding' in order            
 *******************************************************************************************/

task('reset', series('delete-branding', 'reset-branding', ));