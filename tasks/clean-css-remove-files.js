const {
    task,
  } = require('gulp');
const del = require('del');

task('clean-css-remove-files', async (done) => {
  await new Promise((resolve) => {
    del([
        'branding/theme/*.css',
        '!branding/theme/style.css',
        
      ])
      .then(resolve)
  });
  done();
});

