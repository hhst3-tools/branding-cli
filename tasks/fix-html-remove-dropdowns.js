const cheerio = require('gulp-cheerio');
const {
  src,
  dest,
  task
} = require('gulp');


async function removeDropdowns(cb) {
  await new Promise((resolve, reject) => {
    src(['branding/*.htm', 'branding/*.html'])
      .pipe(cheerio(function ($, file, done) {
        $('li').children('ul').each(function (i, elem) {
          $(this).remove();
        })
        done();
      }))
      .pipe(dest('branding/'))
      .on('error', reject)
      .on('finish', resolve);
  })

  cb();
};

task('fix-html-remove-dropdowns', removeDropdowns);