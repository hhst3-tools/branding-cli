'use strict';
const del = require('del');
const {
  task,
  series
} = require('gulp');

async function deleteDownloads(cb) {
  await new Promise(resolve => {
    del([
      'downloads/**/*'
    ]);

    return resolve();
  });

  cb();
}

async function deleteHolding(cb) {
  await new Promise(resolve => {
    del([
      'holding/**/*'
    ]);

    return resolve();
  });

  cb();
}

async function deleteBranding(cb) {
  await new Promise(resolve => {
    del([
      'branding/**/*',
      '!branding/.git',
    ]);

    return resolve();
  });

  cb();
}

async function deleteTemplates(cb) {
  await new Promise(resolve => {
    del([
      'downloads/**/*'
    ]);

    return resolve();
  });

  cb();
}

async function deleteAll(cb) {
  await new Promise(resolve => {
    del([
      'holding/**/*',
      'downloads/**/*',
      'branding/**/*',
      'branding-template/**/*',
      '!branding/.git',
    ]);

    return resolve();
  });

  cb();
}

async function deleteDownloads(cb) {
  await new Promise(resolve => {
    del([
      'downloads/**/*',
    ]);

    return resolve();
  });

  cb();
}

async function deleteTemplates(cb) {
  await new Promise(resolve => {
    del([
      'branding-template/templates/**/*',
    ]);

    return resolve();
  });

  cb();
}


task('clear-downloads', deleteDownloads);
task('clear-branding', deleteBranding);
task('clear-templates', deleteTemplates);
task('clear-holding', deleteHolding);
task('clear-templates', deleteTemplates);