'use strict';
const {
  task,
  series,
  src,
  dest
} = require('gulp');
const phantomHtml = require('website-scraper-phantom');
const scrape = require('website-scraper');
const { argv, on } = require('process');
const spawnSync = require('child_process').spawnSync;
const cssimport = require("gulp-cssimport");
const through2 = require('through2');
const fs = require('fs-extra');

task('scrape', async (done) => {
  await new Promise(async function (resolve, reject) {
    if(argv[2] === "--httrack" || argv[3] === "--httrack" ) return resolve();
    
    let url = process.argv[3];
    if(url === "--url") {
      url = process.argv[4]
    }

    const urlArray = url.split('/');
    const websitename = urlArray[2];
    const options = {
      urls: [url],
      directory: `downloads/${websitename}`,
      httpResponseHandler: phantomHtml,
      ignoreErrors: true,
      request: {
        headers: {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36'
        }
      },
      subdirectories: [{
        directory: '../../branding/theme',
        extensions: ['.jpg', '.png', '.woff', '.woff2', '.css', '.gif', '.svg']
      }, {
        directory: '/delete',
        extensions: ['.js', '.orig']
      }, {
        directory: '../../branding/',
        extensions: ['.html', '.htm']
      }]
    };

    console.log("Downloading website with these options: ");
    console.log(options);
    try {
      await scrape(options);
    } catch (err) {
      return reject(err)
    }
    return resolve();
  });

  done();
});

task('httrack-pull-site', async(done) => {
  await new Promise(async(resolve, reject) => {
    if(argv[2] === "--url" || argv[3] === "--url" ) return resolve();
    
    let url;
    argv.forEach(arg => {
      arg.startsWith('http://');
      url = arg;
    })
    try {
      spawnSync(`httrack`, [`--mirror`, `${url}`, '-O', './downloads', '-N4', '-n', '--near', '-k'], { stdio: 'ignore' })
    } catch(err) {
      return reject(err);
    }
    return resolve()
  })
  done();
});

task('httrack-theme-to-branding', async (done) => {
  await new Promise((resolve, reject) => {
    if(argv[2] === "--url" || argv[3] === "--url") return resolve();
    src(['downloads/web/css/*.css',
      'downloads/web/ico/*.ico', 
      'downloads/web/jpg/*.jpg', 
      'downloads/web/woff2/*.woff2', 
      'downloads/web/woff/*.woff', 
      'downloads/web/gif/*.gif', 
      'downloads/web/svg/*.svg' ])
      .pipe(dest('branding/theme/'))
      .on('error', reject)
      .on('finish', resolve)
  })
  done();
});

task('httrack-html-to-branding', async (done) => {
  await new Promise((resolve, reject) => {
    if(argv[2] === "--url" || argv[3] === "--url") return resolve();
    src([ 'downloads/web/index.html', '/downloads/web/*.html' ])
      .pipe(dest('branding/'))
      .on('error', reject)
      .on('finish', resolve)
  })  
  done();
})

task('fix-relative-imports', async (done) => {
  await new Promise((resolve, reject) => {
    src('branding/theme/*.css')
    .pipe(through2.obj((file, _, cb) => {
       console.log(file.path.toString());
       cb();
    }))
    .pipe(dest(['branding/theme']))
    .on('finish', resolve)
    .on('error', reject)
  })
  done();
})

task('css-import-download', async (done) => {
  await new Promise((resolve, reject) => {
    src("branding/theme/*.css")
        .pipe(cssimport({}))
        .pipe(dest("branding/theme"))
        .on('error', reject)
        .on('finish', resolve);
  })
})


task('httrack', series('httrack-pull-site', 'httrack-theme-to-branding', 'httrack-html-to-branding', 'fix-relative-imports', 'css-import-download'))

task('get-website', series('scrape', 'httrack'))