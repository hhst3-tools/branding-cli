'use strict';
var beautify = require('gulp-beautify');
const {
  src,
  dest,
  task,
  series
} = require('gulp');

task('beautify-css', async (done) => {
  await new Promise((resolve, reject) => {
    src('branding/theme/*.css')
    .pipe(beautify.css({ indent_size: 2 }))
    .pipe(dest('branding/theme/'))
    .on('error', reject)
    .on('finish', resolve);
  });
  done();
});

task('beautify-html', async (done) => {
    await new Promise((resolve, reject) => {
      src('branding/*.htm', 'branding/*.html' )
      .pipe(beautify.html({ indent_size: 2 }))
      .pipe(dest('branding/'))
      .on('error', reject)
      .on('finish', resolve);
    });
    done();
  });

task('clean-beautify', series('beautify-css', 'beautify-html'));