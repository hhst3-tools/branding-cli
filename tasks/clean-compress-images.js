const { task,
dest,
src } = require('gulp');

const imagemin = require('gulp-imagemin');
 
task('clean-compress-images', async (done) => {
    await new Promise( (resolve, reject) => {
    src('branding/theme/*.jpg', 'branding/theme/*.png', 'branding/theme/*.gif', 'branding/theme/*.svg')
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.mozjpeg({quality: 75, progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]}
            )]))
        .pipe(dest('branding/theme'))
        .on('error', reject)
        .on('finish', resolve);
    })
    done();
})
            