'use strict';

const cheerio = require('gulp-cheerio');
const fs = require('fs');
const {
  src,
  dest,
  task
} = require('gulp');

async function extractCss(cb) {
  await new Promise((resolve, reject) => {
    src(['branding/*.htm', 'branding/*.html'])
      .pipe(cheerio(function ($, file, done) {
        if ($('style').length > -1) {
          $('style').each(function (i, elem) {
            let string = $(this).text();
            let ws = fs.createWriteStream('branding/theme/inlineStyle.css', {
              'flags': 'a'
            });
            ws.write(string);
            ws.on('finish', function () {
              console.log('style extracted');
            });
            ws.end();
            $(this).remove();
          });

          $('head').append('<link rel="stylesheet" href="theme/inlineStyle.css" type="text/css" media="all">');
        }
        done();
      }))
      .pipe(dest('branding/'))
      .on('error', reject)
      .on('finish', resolve);
  });

  cb();
}

task('fix-html-extract-inline-css', extractCss);