const { task, src, dest } = require("gulp");
const cheerio = require('gulp-cheerio');

task('validate-rootdiv-check', async(done) => {
    await new Promise((resolve, reject) => {
        src(['branding/*.htm', 'branding/*.html'])
        .pipe(cheerio(($, file, cb) => {
            if($('#rootDiv').length === -1) throw new Error('<div id="rootDiv"></div> not found. Insert Element.');
            $("#rootDiv").attr('style', "width: 1400px; margin: 15px auto;");
            
            cb();
        }))
        .pipe(dest('branding/'))
        .on('error', reject)
        .on('finish', resolve)
    })
    done();
});
