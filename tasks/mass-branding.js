/* eslint-disable no-restricted-syntax */
/* eslint-disable func-names */
/* eslint-disable prefer-arrow-callback */
const through2 = require('through2');
const {
  src,
  dest,
  task,
  series,
} = require('gulp');

const path = require('path');
const fs = require('fs');
const copydir = require('copy-dir');
const jsdom = require('jsdom');
const request = require('request');
const cheerio = require('cheerio');
const del = require('del');
const exec = require('child_process').execSync;
const Commands = require('../scripts/commands.js');
const args = new Commands(process.argv).map;
const {
  JSDOM
} = jsdom;

var git = require('gulp-git');

function readConfigFile() {
  return new Promise((resolve, reject) => {
    fs.readFile('./branding/build/config.json', (err, data) => {
      return !err ? resolve(JSON.parse(data)) : reject(err);
    });
  });
}

function requestData(branchUrl, model) {
  console.log(branchUrl);
  return new Promise((resolve, reject) => {
    request(branchUrl, {
      rejectUnauthorized: false
    }, (err, resp, html) => {
      if (err) return reject(new Error(err));

      const $ = cheerio.load(html);
      let dom = new JSDOM(html);

      let data = {
        url: branchUrl
      }

      //Loops through the different elements of the model.
      //Elements - Objects that hold data about a specific instance of a element type.
      //Element - Objects defined by the user that represent a resource. Example of a Element type is image, css, text, svg
      //Selectors - Each element has an array of selectors which are specifc keys chosen from the website to pull data from
      //
      //     
      //config.json 
      model.elements.forEach((element) => {
        if (element.type === "css") {
          element.selectors.forEach(select => {
            let htmlClass = dom.window.document.querySelector(select.selector);
            console.log(htmlClass);
            let bg = dom.window.getComputedStyle(htmlClass, null).getPropertyValue("background-color");
            data[select.key] = bg;
          });
        }

        if (element.type === "text") {
          element.selectors.forEach(select => {
            data[select.key] = $(select.selector).text();
          })
        }

        if (element.type === "image") {
          element.selectors.forEach(select => {
            data[select.key] = $(select.selector).attr("src");
          })
        }

        if (element.type === "link") {
          element.selectors.forEach((select) => {
            data[select.key] = $(select.selector).attr("href");
          });
        }

        if (element.type === "html") {
          element.selectors.forEach((select) => {
            data[select.key] = $(select.selector).html();
          });
        }

        if (element.type === "icon") {
          element.selectors.forEach((select) => {
            data[select.key] = $(select.selector).parent().attr("href");
          });
        }
      });
      return resolve(data);
    });
  });
}

/******** 
 * GetWebData - Calls the web screaper requestData and returns locations unique information
 * Input: urlsAndBranchs: urls array located in config.json
 *        model: object that slectors to element types on the page
 * ouput: Promise that resolves an object that contains all unique information for a specific location
 *********/
function GetWebData(urlsAndBranches, model) {
  return new Promise(async (resolve, reject) => {
    try {
      let locations = [];
      for (let location of urlsAndBranches) {
        let locationConfig = await requestData(location.url, model);

        for (let key in location) {
          if (location.hasOwnProperty(key)) {
            locationConfig[key] = location[key];
          }
        }

        locations.push(locationConfig);
      }
      resolve(locations);
    } catch (err) {
      reject(err)
    }
  });
}


function loadModifyTemplate() {
  return new Promise((resolve, reject) => {
    let modifyTemplate = require('../branding/build/scripts/modify-template.js');
    return modifyTemplate ? resolve(modifyTemplate) : reject(modifyTemplate);
  })
}

//Checkout Template Branch
//--client client get repo name/folder 
//--template branch used for template 
async function checkout(branch) {
  return new Promise((resolve, reject) => {
    try {
      let brandingFolderPath = path.resolve('./branding');
      let templateBranch = `${args.get('--client')}/${branch}`;

      console.log(`cd ${brandingFolderPath} && git checkout "${templateBranch}" \n\n`);
      exec(`cd ${path.resolve('./branding')} && git checkout "${templateBranch}"`);
      return resolve();
    } catch (err) {
      reject(err);
    }

  }).then(() => {
    return new Promise(async (resolve, reject) => {
      try {
        await copyTemplateToBranding(branch)
        return resolve();
      } catch (err) {
        return reject(err);
      }
    })
  }).then(() => {
    return new Promise(async (resolve, reject) => {
      try {
        await addFiles()
        return resolve();
      } catch (err) {
        return reject(err);
      }
    })
  }).then(() => {
    return new Promise(async (resolve, reject) => {
      try {
        await commitMesseage()
        return resolve();
      } catch (err) {
        return reject(err);
      }
    })
  })
}

function copyTemplateToBranding(branch) {
  return new Promise(async (resolve) => {
    console.log(`COPYING: ./branding-template/templates/${branch}/  to:  ${path.resolve('./branding')}`);
    copydir.sync(`./branding-template/templates/${branch}/`, `${path.resolve('./branding')}`);
    return resolve();
  });
}

function addFiles() {
  return new Promise((resolve) => {
    console.log('RUNNING: git add *');
    exec(`cd ${path.resolve('./branding')} && git add --all`);
    return resolve();
  });
}

function commitMesseage() {
  return new Promise((resolve, reject) => {
    console.log('RUNNING: git commit -m "Updated Branding"');
    let result = exec(`cd ${path.resolve('./branding')} && git commit -m "updated branding"`);

    return resolve();
  });
}

function pushCommits() {
  return new Promise((resolve) => {
    console.log('RUNNING: git push \n\n');
    exec(`cd ${path.resolve('./branding')} && git push --all`);
    return resolve();
  });
}

task('checkout-template-branch', async (done) => {
  await new Promise((resolve) => {
    checkout(args.get('--template'));
    resolve();
  });
  done();
});

task('clear-branding-template', async (done) => {
  await new Promise((resolve) => {
    del(['./branding-template'], {
      dot: true,
    }).then(resolve);
  });
  done();
});

task('clear-old-templates', async (done) => {
  await new Promise((resolve) => {
    del(['./branding-template/templates'], {
      dot: true,
    }).then(resolve);
  });
  done();
});

/************* 
*Task: scrape-web - calls the webscraper GetWebData that an array of unique location data 
                    and rewrites config.json with an added location array containing data for each location
*Input: ./branding/build/config.json
*Output: Updated config containing json data for each location
**************/
task('scrape-web', async (done) => {
  await new Promise((resolve) => {
    src('./branding/build/config.json')
      .pipe(through2.obj(async (file, _, cb) => {
        // Scrape web for each location in url list
        try {
          let config = JSON.parse(file.contents.toString());  
          config.locations = await GetWebData(config.urls, config.model);
          file.contents = Buffer.from(JSON.stringify(config));
          
          cb(null, file)
        } catch (err) {
          throw err;
        }
      })).pipe(through2.obj((file, _, cb) => {
        //Save location data to config
        fs.writeFileSync(file.path, file.contents.toString('utf8'));
        cb();
      }))
      .on('finish', resolve);
  });
  done();
});

task('make-template-folders', async (done) => {
  await new Promise((resolve) => {
    src('./branding/build/config.json')
      .pipe(through2.obj(async (file, _, cb) => {
        let config = JSON.parse(file.contents.toString());
        config.urls.forEach((branch) => {
          copydir.sync('./branding/', `./branding-template/templates/${branch.branch}`, (stat, filepath) => {
            if (stat === 'file' && filepath.includes('.git')) {
              return false;
            }

            if (stat === 'directory' && filepath.includes('\\build')) {
              return false;
            }

            if (stat === 'directory' && filepath.includes('\\templates')) {
              return false;
            }

            return true;
          });
        });
        cb();
      }))
      .on('finish', resolve);
  });
  done();
});

function getBranchNameFromPath(path) {
  const branch = path.split('\\');
  return branch[branch.length - 2];
}

function appendSettingsToFile(file) {
  return new Promise(async resolve => {
    const config = await readConfigFile();
    file.branch = getBranchNameFromPath(file.path);
    file.settings = config.locations.find(loc => loc.branch.includes(file.branch));
    file.global = config.global;
    return resolve(file);
  });
}
task('modify', async (done) => {
  await new Promise((resolve) => {
    src('./branding-template/templates/**/*.htm')
      .pipe(through2.obj(async (file, _, cb) => {
        // Get branch name and setting for locations
        await new Promise(async res => {
          file = await appendSettingsToFile(file);
          return res();
        });
        if (!file.settings) {
          console.error(`"Unable to get settings for ${file.path}"`);
        }
        cb(null, file)
      }))
      .pipe(through2.obj(async (file, _, cb) => {
        // Modify branding-template/template/{branchName}/template.htm
        await new Promise(async modifyResolve => {
          let ModifyTemplate, template;
          try {
            console.log(file.settings);
            ModifyTemplate = await loadModifyTemplate();
            template = new ModifyTemplate(file.settings, file.global, file.contents.toString());
            file.contents = Buffer.from(await template.UpdateTemplate());
          } catch (err) {
            throw err;
          } finally {
            return modifyResolve()
          }
        })
        cb(null, file)
      }))
      .pipe(through2.obj(async (file, _, cb) => {
        // Modify branding-template/template/{branchName}/template.htm
        await new Promise((writeFileResolve) => {
          fs.writeFileSync(file.path, file.contents.toString())
          return writeFileResolve();
        })
        cb();
      }))
      .on('finish', resolve);
  });
  done();
});

function validateThemes(branch) {
  return new Promise(async (resolve, reject) => {
    const {
      stdout,
      stderr,
    } = await exec(`cd ./branding-template/templates/${branch} && themer -v`);
    console.log(stdout);
    return !stderr ? resolve({
      valid: true,
      branch,
    }) : reject(new Error(`Unable to validate: ${branch}`));
  });
}

task('validate-templates', async (done) => {
  await new Promise((resolve) => {
    src('./branding/build/config.json')
      .pipe(through2.obj(async (file, _, cb) => {
        let config = JSON.parse(file.contents);

        await Promise.all(config.urls.map(async location => validateThemes(location.branch)))
          .then((completed) => {
            console.log(completed);
          }).catch(err => {
            throw new Error(err)
          }).finally(cb)
      }))
      .on('finish', resolve);
  });
  done();
});

task('commit-brandings', async (done) => {
  await new Promise((resolve) => {
    src('./branding/build/config.json')
      .pipe(through2.obj(async function (file, _, cb) {

        const config = JSON.parse(file.contents);
        for (const branch of config.urls) {
          this.push(branch);
        }
        cb();
      }))
      .pipe(through2.obj(async (file, _, cb) => {
        await checkout(file.branch).then(async () => {
            await new Promise(async resolve => {
              await pushCommits().then(resolve);
            })
          })
          .catch(err => console.log(err));
        cb();
      }))
      .on('finish', resolve);
  });
  done();
});

task('mass-brandings', series('checkout-template-branch', //'clear-old-templates', 
      'make-template-folders', 
      'scrape-web', 
      'modify'));

task('modify-templates', series(

  'make-template-folders',
  'modify'))