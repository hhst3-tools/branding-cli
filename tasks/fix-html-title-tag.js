const { task, src, dest } = require("gulp");
const cheerio = require('gulp-cheerio');
var readlineSync = require('readline-sync');

task('fix-html-title-tag', async(done) => {
    await new Promise((resolve, reject) => {
        src(['branding/*.htm', 'branding/*.html'])
        .pipe(cheerio(($, file, cb) => {
            if($('title').length == 0) {
                console.log("<title> tag is missing and required: ")
                var titleText = readlineSync.question("Please enter text for <title>: ");
                var titleTag = "<title>"+titleText+"</title>";
                $('head').prepend(titleTag);
            }
            cb();
        }))
        .pipe(dest('branding/'))
        .on('error', reject)
        .on('finish', resolve)
    })
    done();
});
