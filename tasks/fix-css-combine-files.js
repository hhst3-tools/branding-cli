
var concatCss = require('gulp-concat-css');
const cheerio = require('gulp-cheerio');

const {
    src,
    dest,
    task,
    series
  } = require('gulp');
const purgecss = require('gulp-purgecss')

task('combine-css', async (done) => {
  try {
    await new Promise((resolve, reject) => {
    src('branding/theme/*.css')
    .pipe(concatCss("style.css"))
    .pipe(dest('branding/theme/'))
    .on('error', (err) => {
      
      if(err.message.contains("missing {")) {
        resolve();  
      } else {
        reject(err)
      }
      }).on('finish', resolve);
    });
  } catch (err) {
    console.log(err)
  }
  done();
});

task('remove-html-link-tags', async (done) => {
  await new Promise((resolve, reject) => {
    src(['branding/*.htm', 'branding/*.html'])
    .pipe(cheerio(function ($, file, cb) {
    
        $('link').each(function (i, el) {
          $(el).remove();
        });

        $('head').append('<link rel="stylesheet" href="theme/style.css" type="text/css">');
    
      cb();
    }))
    .pipe(dest('branding/'))
    .on('error', reject)
    .on('finish', resolve);
  });
  done();
});

task('fix-css-combine-files', series('combine-css', 'remove-html-link-tags'));