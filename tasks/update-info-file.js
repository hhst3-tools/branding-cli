const {
  task
} = require('gulp');
const fs = require('fs');


let updateInfoTxt = async (done) => {
  await new Promise((resolve, reject) => {
    try {
      fs.writeFileSync('branding/info.txt', process.argv[3]);
      return resolve();
    } catch (err) {
      reject(err);
    }
  })
  done();
}


task('update-info-file', updateInfoTxt)