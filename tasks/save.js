const del = require('del');
const {
  src,
  dest,
  task,
  series
} = require('gulp');


task('delete-holding', async (done) => {
  await new Promise(resolve => {
    del('./holding/**/*').then(resolve)
  });

  done();
})

task('save-branding', async (cb) => {
  await new Promise(resolve => {
    src(['branding/**/*', '!branding/.git']).
    pipe(dest('holding'))
      .on('finish', resolve);
  });

  cb();
})

task('save', series('delete-holding', 'save-branding'));