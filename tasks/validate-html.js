const {
    src,
    dest,
    task,
    series
  } = require('gulp');
const fs = require('fs');
const path = require('path');
const htmlValidator = require('gulp-w3c-html-validator');
const { resolve } = require('path');
const through2 = require('through2');
const htmlTagValidator = require('html-tag-validator');
const { timeStamp, Console } = require('console');
const cheerio = require('gulp-cheerio');


task('required-file-check', async (done) => {
    await new Promise((resolve, reject) => {
        ['./branding/template.htm', './branding/variables.scss', './branding/theme/style.css'].forEach(file => {
            
            if(fs.existsSync(path.resolve(file))) {
                console.info(`found: ${file}`);
            } else {
                reject();
                console.log("test")
            }
        });
        resolve();
    });
    done();
})

task('validate-html-tags', async (done) => {
    await new Promise((resolve, reject) => {
        src('branding/*.htm')
         .pipe(htmlValidator({ skipWarnings: true }))
         .pipe(htmlValidator.reporter())
         .pipe(through2.obj(async(html, _, cb) => {
            try {
                await htmlTagValidator(html.contents.toString('utf-8'), err => {
                    
                    if(err  && !err.toString().includes('path is not a valid self closing tag')) throw new Error(err);
                    console.info("Passed HTML Tag Validation");             
                });
            } catch (err) {
                console.log("line: " + err);
            }

            cb();
        }))
         .on('error', reject)
         .on('finish', resolve);
    })
    done();
});

task('validate-html-content', async(done) => {


    await new Promise((resolve, reject) => {
        src(['branding/*.htm', 'branding/*.html'])
        .pipe(cheerio(function ($, file, cb) {
            isValid = true;
            var logError = (err, el) => {
                console.error(`\nFound Error ${err}:\n${$(el)}`)
                isValid = false;
            }

            $('a[href]').each((_, el) => {
                if (el.attribs.href.startsWith('mailto:')) {
                  logError("Email Link", el);
                } else if (el.attribs.href.startsWith('tel:')) {
                  logError("Telephone Link", el);
                } else if (!el.attribs.href.startsWith('http') && el.attribs.href !== '/') {
                  logError("Relative Link", el);
                } 
            });
            
            if ($('script').length > 0) {
                logError("Javascript Found", 'script');
            } 

            if ($('style').length > 0) {
               logError("Inline CSS styles found", 'style') 
            }

            if ($('title').length == 0) {
                logError("Title Tag is missing", 'title') 
            }
 
            if ($('meta').not('meta[charset]').length > 0) {
                logError("Non charset meta tags found", 'meta');
            }

            if ($('meta[charset]').length == 0) {
                logError("charset meta tag not found found", 'meta');
            }
            
            if(!isValid) throw new Error("HTML Invalid");
            

            cb();
        }))
        .on('error', reject)
        .on('finish', resolve)
    })
    done();
})

task('validate-html', series('required-file-check', 'validate-html-tags', 'validate-html-content' ))
