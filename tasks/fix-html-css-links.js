'use strict';
const cheerio = require('gulp-cheerio');
const {
  src,
  dest,
  task
} = require('gulp');

async function fixCssLinks(cb) {
  await new Promise((resolve, reject) => {
    src(['branding/*.htm', 'branding/*.html'])
      .pipe(cheerio(function ($, file, done) {
        $('link').each(function (i, elem) {
          let re = /^(.(.*\.css))*$/;
          let filepath = $(this).attr('href');
          if (!filepath.match(re)) {
            $(this).remove();
          } else {
            let url = $(this).attr('href').split('/');
            let filename = url[url.length - 1];
            let removeVersion = filename.split('@');
            let cleanFilename = removeVersion[0];
            console.log(filename);
            $(this).attr('href', `theme/${cleanFilename}`);
          }
        });
        done();
      }))
      .pipe(dest('branding/'))
      .on('error', reject)
      .on('finish', resolve);
  });

  cb();
}

task('fix-html-css-links', fixCssLinks);