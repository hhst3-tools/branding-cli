'use strict';
const cheerio = require('gulp-cheerio');
const {
  src,
  dest,
  task,
  series
} = require('gulp');
var readlineSync = require('readline-sync');

task('fix-img-src', async(done) => {
  await new Promise((resolve, reject) => {
    src(['branding/*.htm', 'branding/*.html'])
      .pipe(cheerio(function ($, file, cb) {
        $('img').each(function (i, elem) {
          let srcUrl = $(this).attr('src');
          if (srcUrl !== undefined) {
            let urlArray = srcUrl.split('/');
            let fileName = urlArray[urlArray.length - 1];
          
            $(this).attr('src', 'theme/' + fileName);
          }
        });
        cb();
      }))
      .pipe(dest('branding/'))
      .on('error', reject)
      .on('finish', resolve);
  });
  done();
});
  

task('fix-img-alt', async(done) => {
  await new Promise(resolve => {
    src(['branding/*.htm', 'branding/*.html'])
    .pipe(cheerio(function ($, file, cb) {
      //Look for all img tags missing the alt attribute
      //ask user for text
      //insert alt attribute 
      $('img').not('img[alt]').each((_, el) => {
        console.log(`<img> tag mising alt: ${$(el)}`)
        var altString = readlineSync.question("Enter alt text:\n");
        $(el).attr('alt', altString)
      })
      cb();
    }))
    .pipe(dest('branding/'))
    .on('finish', resolve)
    })
  done();
});

task('fix-html-img-tags', series('fix-img-src', 'fix-img-alt'));