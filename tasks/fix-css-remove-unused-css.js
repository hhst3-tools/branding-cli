'use strict';
const {
    src,
    dest,
    task
  } = require('gulp');
const purgecss = require('gulp-purgecss')

async function removeUnusedCss(done) {
  await new Promise((resolve, reject) => {
    src('branding/theme/*.css')
    .pipe(purgecss({
        content: ['branding/template.htm']
    }))
    .pipe(dest('branding/theme/'))
    .on('error', reject)
    .on('finish', resolve)
  });
  done();
}

task('fix-css-remove-unused-css', removeUnusedCss);