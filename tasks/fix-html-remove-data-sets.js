const cheerio = require('gulp-cheerio');
const {
  src,
  dest,
  task
} = require('gulp');

function camelToDash(str) {
  return str.replace(/([A-Z])/g, (g) => `-${g[0].toLowerCase()}`);
}

let removeDataSets = async (cb) => {
  await new Promise((resolve, reject) => {
    src(['branding/*.htm', 'branding/*.html'])
      .pipe(cheerio(function ($, file, done) {
        $('*').each(function (i, elem) {
          let dataSet = $(this).data();
          if (Object.keys(dataSet).length !== 0) {
            for (let key in dataSet) {
              let dashed = camelToDash(key);
              console.log(dashed);
              $(this).removeAttr(`data-${dashed}`);
            }
          }
        });
        done();
      }))
      .pipe(dest('branding/'))
      .on('error', reject)
      .on('finish', resolve);
  });

  cb();
}

task('fix-html-remove-data-sets', removeDataSets);