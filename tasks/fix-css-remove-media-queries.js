const {
    src,
    dest,
    task
  } = require('gulp');
const PostCSS = require("gulp-postcss");

task('fix-css-remove-media-queries', async (done) => {
    await new Promise((resolve, reject) => {
        src('branding/theme/*.css')
        .pipe(PostCSS([
                require('postcss-unmq')({ 
                    type: 'screen',
                    width: 1300,
                    height: 868,
                    resolution: '1dppx',
                    color: 3
                 })
            ]))
        .pipe(dest('branding/theme/'))
        .on('error', reject)
        .on('finish', resolve) });
    done();
});