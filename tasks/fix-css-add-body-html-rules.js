const { task, src, dest } = require("gulp");
const gap = require('gulp-append-prepend');
 

task('fix-css-add-body-html-rules', async(done) => {
    await new Promise((resolve, reject) => {
    
        src('branding/theme/style.css')
            .pipe(gap.prependText('body, html { margin: 0; padding: 0; background-color: #fff}'))
            .pipe(dest('branding/theme/'))
            .on('error', reject)
            .on('finish', resolve)
    })
    done();
});

