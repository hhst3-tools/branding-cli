const {
  task
} = require('gulp');
const fs = require('fs');


let renameIndex = async (done) => {
  await new Promise((resolve, reject) => {
    url = process.argv[3];
    try {
      fs.writeFileSync('branding/info.txt', url);
      if (!fs.existsSync('branding/template.htm')) {
        fs.renameSync('branding/index.html', 'branding/template.htm');
      }
      return resolve();
    } catch (err) {
      reject(err);
    }
  })

  done();
}


task('rename-index', renameIndex)