/* eslint-disable no-restricted-syntax */
const cheerio = require('gulp-cheerio');
const {
  src,
  dest,
  task,
  series,
} = require('gulp');

let stripHtml = async done => {
  await new Promise((resolve, reject) => {
    src(['branding/*.htm', 'branding/*.html'])
      .pipe(cheerio(($, file, cb) => {
      
        const invalidData = {
          tags: ['script', 'noscript', 'iframe', 'input', 'aside'],
          attributes: ['xml:lang','xmlns', 'itemprop', 'lang', 'aria-label', 'itemscope', 'itemtype', 'onclick', 'onchange', 'onmouseover', 'onmouseout', 'onkeydown', 'onload', 'aria-controls', 'aria-haspopup', 'aria-live', 'aria-hidden', 'aria-expanded'],
        };

        invalidData.tags.forEach((tag) => {
          $(tag).each(function (i, el) {
            $(el).remove();
          });
        });

        invalidData.attributes.forEach((attr) => {
          $('*').each((_, el) => {
            $(el).removeAttr(attr, null);
          })
        });

        $('head').prepend('<meta charset="utf-8">')
        cb();
      }))
      .pipe(dest('branding/'))
      .on('error', reject)
      .on('finish', resolve);
  })
  done();
}

task('fix-html-strip-tags', stripHtml);