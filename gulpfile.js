const {
  series,
  task,
  registry
} = require('gulp');

var HubRegistry = require('gulp-hub');
var hub = new HubRegistry(['tasks/*.js']);

registry(hub);


task('save-branding', series('clear-holding', 'save'));
task('reset-branding', series('clear-branding', 'reset'));
task('clear-all-folders', series('clear-branding', 'clear-holding', 'clear-templates', 'clear-downloads'));

task('get-client-website', series(
  'clear-downloads',
  'make-branding-skeleton',
  'get-website',
  'update-info-file',
  'rename-index',
  'clean-beautify'
));

task('fix-html', series(
  'fix-html-strip-tags',
  'fix-html-extract-inline-css',
  'fix-html-links',
  'fix-html-title-tag',
  'fix-html-img-tags',
  'fix-html-meta-tags',
  'fix-html-remove-data-sets',
  'fix-html-remove-dropdowns',
  'fix-html-lint',
  'clean-beautify'
));

task('fix-css', series(
  'fix-css-combine-files',
  'clean-css-remove-files',
  'fix-css-urls',
  'fix-css-remove-media-queries',
  'fix-css-clean',
  'clean-beautify'
));

task('clean-branding', series(
  'fix-html',
  'fix-css',
  'clean-compress-images',
  'clean-remove-unused-images',
  'fix-css-remove-unused-css',
  'fix-css-wrap-css-rules',
  'clean-html-css-namespaces',
  'fix-css-add-body-html-rules',
  'clean-beautify'

));

task('validate', series(
  'save-branding',
  'validate-html',
  'validate-css',
  'validate-rootdiv-check'));

task('default', series(
  'get-client-website',
  'clean-branding',
  'validate'
))